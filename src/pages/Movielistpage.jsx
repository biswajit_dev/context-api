/** @format */

import React, { useContext } from 'react';

import CategoryTitle from '../components/CategoryTitle';
import Movielist from '../components/Movielist';
import { RootContext } from '../context/Rootcontext';

import '../styles/Movielistpage.css';

const Movielistpage = () => {
  const { movieList, loading } = useContext(RootContext);

  return (
    <div className='page-container'>
      {loading
        ? 'Loading...'
        : movieList.map((el) => (
            <div className='list-container' key={el.category}>
              <CategoryTitle title={el.category} />
              <Movielist movies={el.movies} />
            </div>
          ))}
    </div>
  );
};

export default Movielistpage;
