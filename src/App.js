import React from "react";

import { RootProvider } from "./context/Rootcontext";
import Navbar from "./components/Navbar";
import Movielistpage from "./pages/Movielistpage";

function App() {
  return (
    <RootProvider>
      <Navbar />
      <Movielistpage />
    </RootProvider>
  );
}

export default App;
