/** @format */

import React, { useState, createContext } from 'react';

export const RootContext = createContext({});

export const RootProvider = ({ children }) => {
  const [movieList, setMovieList] = useState([]);
  const [loading, setLoading] = useState(false);

  return (
    <RootContext.Provider
      value={{ movieList, setMovieList, loading, setLoading }}>
      {children}
    </RootContext.Provider>
  );
};
