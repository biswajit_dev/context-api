/** @format */

import React, { useEffect, useContext } from 'react';

import '../styles/Navbar.css';
import Searchbar from './Searchbar';
import { RootContext } from '../context/Rootcontext';

const Navbar = () => {
  const { setMovieList, setLoading } = useContext(RootContext);

  useEffect(() => {
    (async () => {
      setLoading(true);
      fetch('http://localhost:4000/movies')
        .then((res) => res.json())
        .then((data) => {
          setMovieList(data);
          setLoading(false);
        })
        .catch((err) => {
          setLoading(false);
          console.log('Error: ', err);
        });
    })();
  }, []);

  return (
    <nav className='bar-container'>
      <div className='title-container'>
        <div className='title-wrapper'>
          <h1 className='nav-title'>WOOKIE MOVIES</h1>
        </div>
      </div>
      <Searchbar />
    </nav>
  );
};

export default Navbar;
