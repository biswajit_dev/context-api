/** @format */

import React from 'react';
import "../styles/imagecontainer.css";

const ImageContainer = ({ url, alt }) => {
  return <img className="image-style" src={url} alt={alt && alt} />;
};

export default ImageContainer;
