/** @format */

import React from 'react';
import SearchIcon from '../assets/search.png';
import '../styles/Search.css';

const Searchbar = () => {
  return (
    <div className='search-container'>
      <img src={SearchIcon} alt='_search-icon' className="search-image" />
      <input className="search-input" />
    </div>
  );
};

export default Searchbar;
