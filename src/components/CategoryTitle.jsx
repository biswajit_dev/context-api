/** @format */

import React from 'react';
import '../styles/Categorytitle.css';

const CategoryTitle = ({ title }) => {
  return (
    <React.Fragment>
      <h3 className='category-title'>{title}</h3>
    </React.Fragment>
  );
};

export default CategoryTitle;
