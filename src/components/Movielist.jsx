/** @format */

import React from 'react';
import ImageContainer from './ImageContainer';
import '../styles/movielist.css';

const Movielist = ({ movies }) => {
  return (
    <div className='movie-list-container'>
      {movies.map((movie) => (
        <div className='movie-container' key={movie.title}>
          <ImageContainer url={movie.poster} alt={`${movie.title}_img`} />
          <div className='desc-container'>
            <h4>{movie.title}</h4>
            <h6>{movie.imdb_rating}</h6>
            <h6>{movie.director}</h6>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Movielist;
