/** @format */

// const Movies = require('../model/movies.model');

// const uniqueArray = (array) => {
//   let uniqueArr = [];
//   array.map((el) =>
//     uniqueArr.filter((e) => e === el).length > 0 ? null : uniqueArr.push(el)
//   );
//   return uniqueArr;
// };

// exports.getMovies = async (req, res) => {
//   try {
//     let moviesCategories = [];
//     const movies = await Movies.find();
//     movies.map((movie) => {
//       moviesCategories = [...moviesCategories, ...movie.genres];
//     });
//     const uniqueCat = uniqueArray(moviesCategories);
//     let catMovie = [];
    
//     // uniqueCat.map(cat => {
//     //     let obj = {};
//     //     movies.map(movie => {
//     //         if(movie.genres.indexOf(cat) >= 0) {
//     //             obj = {
//     //                 category: movie.genres.find(e => e === cat),
//     //                 movies:
//     //             }
//     //         }
//     //     })
//     // })

//     return res.status(200).send(uniqueCat);
//   } catch (error) {
//     return res.sendStatus(500);
//   }
// };

const fs = require("fs");
const path = require("path");
const filePath = path.join(__dirname, "../data/data.json");

exports.getMovies = async (req, res) => {
  try {
    fs.readFile(filePath, (err, data) => {
      if(err) return res.sendStatus(400);
      const fileData = JSON.parse(data.toString());
      return res.send(fileData);
    })
    
  } catch (error) {
    console.log("error", error)
    return res.sendStatus(500);
  }
}
