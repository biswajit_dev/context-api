/** @format */

const mongoose = require('mongoose');

const moviesSchema = new mongoose.Schema({
  backdrop: {
    type: String,
  },
  cast: [
    {
      type: String,
    },
  ],
  classification: {
    type: String,
  },
  director: {
    type: String,
  },
  genres: [
    {
      type: String,
    },
  ],
  imdb_rating: {
    type: Number,
  },
  length: {
    type: String,
  },
  overview: {
    type: String,
  },
  poster: {
    type: String,
  },
  released_on: {
    type: String,
  },
  slug: {
    type: String,
  },
  title: {
    type: String,
  },
});

module.exports = mongoose.model('Movielist', moviesSchema);
