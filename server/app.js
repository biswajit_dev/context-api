/** @format */

const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();
const { getMovies } = require('./controllers/movies.controller');

app.use(cors());

require('dotenv').config();

/**---------DB connection---------*/
mongoose.connect(process.env.ATLAS_URL, (err, connect) => {
  if (err) return console.log(err);
  console.log('DB connected!');
});

/**---------Routes---------*/
app.get('/movies', getMovies);

app.listen(4000, () => console.log('Server running at PORT 4000'));
